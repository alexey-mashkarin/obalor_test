<?php

namespace App\Console\Commands;

use App\Models\Customer;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class CustomersImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'customers:import {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import customers from csv file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $file = $this->argument('file');
            if (!file_exists($file)) {
                throw new \Exception('File "' . $file . '" not found');
            }

            if (substr($file, -4) !== '.csv') {
                throw new \Exception('Unsupported file format');
            }

            $this->info('Import started at ' . (new Carbon())->toAtomString());

            $arReport = [];
            $f = fopen($file, 'r');
            $delimiter = ',';
            $enclosure = '"';
            $escape = '\\';
            $arHeaders = fgetcsv($f, 0, $delimiter, $enclosure, $escape);
            Customer::truncate();
            while ($arRow = fgetcsv($f, 0, $delimiter, $enclosure, $escape)) {
                $arCustomer = array_combine($arHeaders, $arRow);

                try {
                    $customer = new Customer($this->validateCustomer($arCustomer));
                    $customer->save();
                } catch (ValidationException $validationException) {
                    $arReport[] = $arCustomer + [
                        'error' => implode(', ', array_keys($validationException->errors())),
                    ];
                }
            }

            $this->info('Import ended at ' . (new Carbon())->toAtomString());

            if (!empty($arReport)) {
                $this->warn('Import had errors in follow rows:');
                dump($arReport);
            }


        } catch (\Exception $exception) {
            $this->error('Error: ' . $exception->getMessage());
            return 1;
        } finally {
            if (isset($f) && is_resource($f)) {
                fclose($f);
            }
        }

        return 0;
    }

    public function validateCustomer($arCustomer)
    {
        $validator = Validator::make($arCustomer, [
            'id' => 'required|integer',
            'name' => 'required',
            'email' => 'required|email',
            'age' => 'required|integer|between:18,99',
            'location' => 'string'
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return $validator->validated();

    }
}
