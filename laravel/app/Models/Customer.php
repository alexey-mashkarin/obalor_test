<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    const DEFAULT_LOCATION = 'Unknown';

    protected $guarded = [];
    protected $casts = [
        'age' => 'integer',
    ];


    /**
     * Location mutator for empty strings
     */
    public function setLocationAttribute($value)
    {
        $this->attributes['location'] = $value ?: self::DEFAULT_LOCATION;
    }
}
